import datetime
from io import BytesIO

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from flask import Flask, render_template, request, send_file, session
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
from scipy.signal import cspline1d, cspline1d_eval
from werkzeug.contrib.cache import SimpleCache

from config.stars import *
from config.text import *

app = Flask(__name__)
app.secret_key = 'really secret'
cache = SimpleCache(default_timeout=0)
plt.style.use('./config/stylelib/nasvarstar.mplstyle')


"""
Controllers
"""


@app.context_processor
def utility_processor():
    def get_time():
        return datetime.datetime.now().time().isoformat()

    return dict(get_time=get_time)


@app.route('/')
def introduction():
    return render_template('introduction.html',
                           title=intro_title,
                           text=intro_text,
                           session=setup_dataframes(),
                           start=True)


@app.route('/' + estimate_title)
def estimation():
    return render_template('estimation.html',
                           title=estimate_title,
                           text=estimate_text)


@app.route('/' + estimate_title, methods=['POST'])
def slider():
    slider_value = float(request.form['slider'])
    return render_template('estimation.html',
                           title=estimate_title,
                           text=estimate_text,
                           data=add_step(slider_value),
                           slider=slider_value,
                           step=get_step())


@app.route('/step_plot')
def step_plot():
    return generate_plot(cache.get(session.get('user')))


@app.route('/' + results_title)
def results():
    return render_template('results.html',
                           title=results_title,
                           text=results_text,
                           finalp=step_plot())


@app.route('/' + info_title)
def information():
    # probably need a restart function
    cache.delete(session.get('user'))
    return info_title


"""
Model methods
"""


def setup_dataframes():
    session['user'] = cache.inc('session')
    cache.set(session.get('user'), pd.Series())
    if not cache.has('stars_df'):
        cache.set('stars_df', pd.DataFrame([star_one_comparisons]))
    return session['user']


def get_step():
    return cache.get(session.get('user')).last_valid_index()


def add_step(slider_value):
    temp_sr = cache.get(session.get('user'))
    lvi = temp_sr.last_valid_index()
    if lvi:
        temp_sr.set_value(lvi + 1, slider_value)
    else:
        temp_sr.set_value(1, slider_value)
    cache.set(session.get('user'), temp_sr)


def generate_plot(data):
    fig = Figure()
    canvas = FigureCanvasAgg(fig)
    ax = fig.add_subplot(111, xlim=(1, 16), ylim=(10.6, 17.4), ylabel='Mag. (-)')

    if len(data) > 2:
        # possibly use np.array if size known
        ix = data.index.values
        values = data.values
        ix_new = np.linspace(ix.min(), ix.max(), 100)
        csc = cspline1d(values)
        values_new = cspline1d_eval(csc, ix_new, dx=1, x0=ix_new[0])
        ax.plot(ix_new, values_new, ix, values, 'o')

    else:
        ax.plot(data, 'o')

    img_data = BytesIO()
    fig.savefig(img_data)
    img_data.seek(0)
    return send_file(img_data, mimetype='image/png')


"""
End
"""
if __name__ == '__main__':
    app.run()
