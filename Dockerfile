FROM fedora:26

MAINTAINER jalenigaev@gmail.com

ADD https://bitbucket.org/nbastro/nasvarstar/get/HEAD.tar.gz /srv

# docker add needs --strip 1...
RUN mv /srv/nbastro* /srv/nasvarstar && \
    dnf install -y libstdc++ python3-gunicorn python3-tkinter  && \
	pip3 install --no-cache-dir -r /srv/nasvarstar/requirements.txt && \
	dnf clean all

EXPOSE 80

WORKDIR /srv/nasvarstar

# can only use one worker due to python memory cache
CMD python3-gunicorn -w 1 -b 0.0.0.0:80 nasvarstar:app
