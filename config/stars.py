""" Data for NAS variable star app STARS

The formatting is important

"""

# Star one

star_one_name = 'LX Ser'

star_one_explanation = 'Some variable star, ' \
                       'no idea...'

star_one_comparisons = {'A': 10.9, 'B': 11.7, 'C': 14.0, 'D': 14.7,
                        'E': 14.9, 'F': 15.1, 'G': 16.0, 'H': 16.4,
                        'I': 16.8}

star_one_values = {'start': 13.3, 'end': 17.4, 'step': 0.2}

star_one_test = 0


# Star Two
