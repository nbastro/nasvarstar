""" Data for NAS variable star app TEXT

The formatting is important
Titles should not be capitalised

"""

# Introduction page text

intro_title = 'introduction'

intro_text = 'Astronomers measure the brightness (or Magnitude) of a variable star by comparing it with nearby stars called Comparison Stars. These are stars that do not vary in brightness, and have known magnitudes that span the range of magnitudes of the variable. Etc.'


# Estimation page text

estimate_title = 'estimation'

estimate_text = 'A small amount of text could go here...'


# Results page text

results_title = 'results'

results_text = 'Text for page to click on light curve most like the one generated...'


# Information page text

info_title = 'information'

info_text = 'This is the information text'
